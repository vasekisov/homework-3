﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotFound_ReturnNotFound()
        {
            //Arrange
            var partnerMock = new Mock<IRepository<Partner>>();
            partnerMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Partner)null);
            var testPerson = new PartnersController(partnerMock.Object);

            //Act
            var result = await testPerson.SetPartnerPromoCodeLimitAsync(Guid.Empty, new SetPartnerPromoCodeLimitRequest());

            //Assert
            result.Should().BeOfType<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnBadRequest()
        {
            //Arrange
            var partnerMock = new Mock<IRepository<Partner>>();
            var partner = new Partner()
            {
                IsActive = false
            };
            partnerMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var testPerson = new PartnersController(partnerMock.Object);

            //Act
            var result = await testPerson.SetPartnerPromoCodeLimitAsync(Guid.Empty, new SetPartnerPromoCodeLimitRequest());

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>().Which.StatusCode.Should().Be(400);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_HasActiveLimit_SetPartnerNumberIssuedPromoCodesToZero()
        {
            //Arrange
            var activePromoCodeLimit = new PartnerPromoCodeLimit();
            var partner = new Partner()
            {
                NumberIssuedPromoCodes = 123,

                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    activePromoCodeLimit
                },

                IsActive = true
            };

            var partnerMock = new Mock<IRepository<Partner>>();
            partnerMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var testPerson = new PartnersController(partnerMock.Object);

            //Act
            var result = await testPerson.SetPartnerPromoCodeLimitAsync(Guid.Empty, new SetPartnerPromoCodeLimitRequest());

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
            activePromoCodeLimit.CancelDate.Should().NotBeNull();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_HasNotActiveLimit_NotSetPartnerNumberIssuedPromoCodesToZero()
        {
            //Arrange
            var canceledPromoCodeLimit = new PartnerPromoCodeLimit()
            {
                CancelDate = DateTime.Now
            };

            var partner = new Partner()
            {
                NumberIssuedPromoCodes = 123,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    canceledPromoCodeLimit
                },


                IsActive = true
            };

            var partnerMock = new Mock<IRepository<Partner>>();
            partnerMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var testPartner = new PartnersController(partnerMock.Object);

            //Act
            var result = await testPartner.SetPartnerPromoCodeLimitAsync(Guid.Empty, new SetPartnerPromoCodeLimitRequest());

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(123);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsZero_ReturnBadRequest()
        {
            //Arrange
            var partner = new Partner()
            {
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };
            var partnerRepositoryMock = new Mock<IRepository<Partner>>();
            partnerRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var testPartner = new PartnersController(partnerRepositoryMock.Object);
            var partnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest
            {
                Limit = 0
            };
            //Act
            var result = await testPartner.SetPartnerPromoCodeLimitAsync(Guid.Empty, partnerPromoCodeLimitRequest);

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>().Which.Value.Should().Be("Лимит должен быть больше 0");
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_AddLimitToDBB()
        {
            //Arrange
            var partner = new Partner()
            {
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };

            var partnerMock = new Mock<IRepository<Partner>>();
            partnerMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var testee = new PartnersController(partnerMock.Object);
            var partnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest
            {
                Limit = 1
            };

            //Act
            var result = await testee.SetPartnerPromoCodeLimitAsync(Guid.Empty, partnerPromoCodeLimitRequest);

            //Assert
            partner.PartnerLimits.Count.Should().Be(1);
            partnerMock.Verify(x => x.UpdateAsync(partner), Times.Once);
        }
    }

}
